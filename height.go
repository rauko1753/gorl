package gorl

import (
	"math"
	"sort"
)

// GenTileFloat generates Tiles from float64 values.
type GenTileFloat func(o Offset, h float64) *Tile

// heightmap is a grid of float64, with methods for manipulating the heightmap.
type heightmap struct {
	cols, rows int
	buf        [][]float64
}

// GenHeightmap creates a 2D grid of Tile using the given GenTileFloat.
func GenHeightmap(cols, rows int, f GenTileFloat) []*Tile {
	b := make([][]float64, cols)
	for x := 0; x < cols; x++ {
		b[x] = make([]float64, rows)
	}
	h := &heightmap{cols, rows, b}

	h.RaiseEllipses()
	h.Smooth()
	h.Equalize()
	h.Normalize()

	return GenTileGrid(h.cols, h.rows, func(o Offset) *Tile {
		return f(o, h.buf[o.X][o.Y])
	})
}

// RaiseEllipses will randomly raise ellipses on the map, thereby creating
// terrain like height values.
func (h *heightmap) RaiseEllipses() {
	radiusX, radiusY := h.cols/8, h.rows/8
	rx2, ry2 := float64(radiusX*radiusX), float64(radiusY*radiusY)
	numEllipses := h.cols + h.rows

	for i := 0; i < numEllipses; i++ {
		center := RandOffset(h.cols, h.rows)
		for dx := -radiusX; dx <= radiusX; dx++ {
			for dy := -radiusY; dy <= radiusY; dy++ {
				// if outside the ellipse, skip the point
				if float64(dx*dx)/rx2+float64(dy*dy)/ry2 >= 1 {
					continue
				}

				// raise the horizontally wrapped point if it is in bounds
				x, y := Mod(center.X+dx, h.cols), center.Y+dy
				if 0 <= x && x < h.cols && 0 <= y && y < h.rows {
					h.buf[x][y]++
				}
			}
		}
	}
}

// Smooth averages each value of the heightmap with the its neighbors' values.
func (h *heightmap) Smooth() {
	// Note that we directly apply the averages to the map, meaning that
	// previously smoothed values affect the current cell. The effect is
	// negligible for larger maps, so we don't care.
	for x := 1; x < h.cols-1; x++ {
		for y := 1; y < h.rows-1; y++ {
			h.buf[x][y] = (h.buf[x-1][y] + h.buf[x][y] + h.buf[x+1][y]) / 3
			h.buf[x][y] = (h.buf[x][y-1] + h.buf[x][y] + h.buf[x][y+1]) / 3
		}
	}
}

// Equalize performs histogram equalization on the heightmap.
func (h *heightmap) Equalize() {
	// Compute the histogram function.
	hist := make([]float64, h.cols*h.rows)
	for x := 0; x < h.cols; x++ {
		for y := 0; y < h.rows; y++ {
			hist[x+y*h.cols] = h.buf[x][y]
		}
	}
	sort.Float64s(hist)

	// Compute the transfer function from the cumulative distribution.
	cumulative := 0.0
	transfer := make(map[float64]float64)
	for _, height := range hist {
		cumulative += height
		transfer[height] = cumulative
	}

	// Apply the transfer function to the heightmap.
	for x := 0; x < h.cols; x++ {
		for y := 0; y < h.rows; y++ {
			h.buf[x][y] = transfer[h.buf[x][y]]
		}
	}
}

// Normalize maps every value of the heightmap to the range [0, 1].
func (h *heightmap) Normalize() {
	// Compute the min and max heights
	min, max := h.buf[0][0], h.buf[0][0]
	for x := 0; x < h.cols; x++ {
		for y := 0; y < h.rows; y++ {
			min = math.Min(min, h.buf[x][y])
			max = math.Max(max, h.buf[x][y])
		}
	}

	// Normalize the heights to [0, 1].
	span := max - min
	for x := 0; x < h.cols; x++ {
		for y := 0; y < h.rows; y++ {
			h.buf[x][y] = (h.buf[x][y] - min) / span
		}
	}
}
