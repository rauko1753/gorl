package gorl

import (
	"testing"
)

type StrGrid []string

func (g StrGrid) FoVCase() (*Tile, map[Offset]*Tile) {
	cols, rows := len(g[0]), len(g)
	var origin *Tile
	fov := make(map[Offset]*Tile)
	GenTileGrid(cols, rows, func(o Offset) *Tile {
		t := NewTile(o)
		ch := g[o.Y][o.X] // care it really is y, x (not x, y)
		t.Face = Char(rune(ch))
		switch ch {
		case '@':
			origin = t
			fov[t.Offset] = t
		case '*':
			fov[t.Offset] = t
		case '%':
			fov[t.Offset] = t
			t.Pass = false
			t.Lite = false
		case '#':
			t.Pass = false
			t.Lite = false
		}
		return t
	})
	expected := make(map[Offset]*Tile)
	for off, tile := range fov {
		expected[off.Sub(origin.Offset)] = tile
	}
	return origin, expected
}

func FoVEquals(actual, expected map[Offset]*Tile) bool {
	if len(actual) != len(expected) {
		return false
	}
	for off, atile := range actual {
		if etile, ok := expected[off]; !ok || atile != etile {
			return false
		}
	}
	return true
}

func TestFoV(t *testing.T) {
	cases := []struct {
		g StrGrid
		r int
	}{
		{
			StrGrid{
				"%%%%%##",
				"%****.#",
				"%*@**.#",
				"%****.#",
				"%****.#",
				"#######",
			}, 2,
		}, {
			StrGrid{
				"%%%%%%##",
				"%*****.#",
				"%*@***.#",
				"%*****.#",
				"%*****.#",
				"%*****.#",
				"#......#",
				"########",
			}, 3,
		}, {
			StrGrid{
				"%%%%%%%%%%#####",
				"%**********...#",
				"%**********...#",
				"%*@***%.......#",
				"%**********...#",
				"%**********...#",
				"%%%%%%%%%%#####",
			}, 8,
		}, {
			StrGrid{
				"%%%%%%%%%%#####",
				"%**********...#",
				"%*****%.......#",
				"%*@***%.......#",
				"%**********...#",
				"%**********...#",
				"%%%%%%%%%%#####",
			}, 8,
		}, {
			StrGrid{
				"%%%%%%#########",
				"%****.........#",
				"%***..........#",
				"%*@%..........#",
				"%***..........#",
				"%****.........#",
				"%%%%%%#########",
			}, 8,
		},
	}
	for i, c := range cases {
		origin, expected := c.g.FoVCase()
		actual := FoV(origin, c.r)
		if !FoVEquals(actual, expected) {
			t.Errorf("FoV cased %d failed", i)
		}
	}
}

func (g StrGrid) LoSCase() (origin, goal *Tile, expected map[*Tile]struct{}) {
	cols, rows := len(g[0]), len(g)
	expected = make(map[*Tile]struct{})
	GenTileGrid(cols, rows, func(o Offset) *Tile {
		t := NewTile(o)
		ch := g[o.Y][o.X]
		t.Face = Char(rune(ch))
		switch ch {
		case '@':
			origin = t
		case '$':
			goal = t
			expected[t] = struct{}{}
		case 'x':
			expected[t] = struct{}{}
		case '#':
			t.Lite = false
		}
		return t
	})

	if len(expected) <= 1 {
		return origin, goal, nil
	}
	return origin, goal, expected
}

func LoSEquals(actual []*Tile, ok bool, expected map[*Tile]struct{}) bool {
	if expected == nil && (ok || actual != nil) {
		return false
	}
	if expected != nil && (!ok || actual == nil) {
		return false
	}

	actualSet := make(map[*Tile]struct{})
	for _, tile := range actual {
		actualSet[tile] = struct{}{}
	}

	if len(actualSet) != len(expected) {
		return false
	}
	for tile := range expected {
		if _, ok := actualSet[tile]; !ok {
			return false
		}
	}

	return true
}

func TestLoS(t *testing.T) {
	cases := []StrGrid{
		{
			"###############",
			"#.............#",
			"#..@xxxxxxx$..#",
			"#.............#",
			"###############",
		}, {
			"###############",
			"#.............#",
			"#..@...#...$..#",
			"#.............#",
			"###############",
		}, {
			"###############",
			"#.............#",
			"#..@x###......#",
			"#....xxxxxxxx$#",
			"###############",
		}, {
			"###############",
			"#.............#",
			"#..$.###......#",
			"#............@#",
			"###############",
		},
	}
	for i, g := range cases {
		origin, goal, expected := g.LoSCase()
		actual, ok := LoS(origin, goal)
		if !LoSEquals(actual, ok, expected) {
			t.Errorf("LosTrace case %d failed", i)
		}
	}
}
