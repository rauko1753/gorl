package gorl

import (
	"fmt"
	"regexp"
	"strings"
	"unicode"
)

// Log applies the gorl log formating language to create a LogEvent.
//
// The format specifiers include the following:
// 	%s - subject
// 	%o - object
// 	%v - verb
// 	%x - literal
// Additionally, verb literals may be included using the form <verb>.
//
// Each format specifier can be mapped to any arbitrary value, and is converted
// to a string by the fmt package. Consequently, format values should probably
// implement the fmt.Stringer interface to ensure that the values are correctly
// represented in the formatted string.
//
// Example usage:
// 	Fmt("%s <hit> %o", hero, bear) yields "You hit the bear."
// 	Fmt("%s %v %o", tiger, verb, hero) yields "The saber-tooth slashes you."
// 	Fmt("%s <hit> %o!", tiger, rabbit) yields "The saber-tooth hits the rabbit!"
// 	Fmt("%s %v %o?", bear, verb, bear) yields "The bear hits itself?"
// 	Fmt("%s <laugh>", unique) yields "Gorp laughs."
//
// Note that if the String conversion for a value is "you" so that the formatter
// knows which grammatical-person to use. Named monsters should have string
// representations which are capitalized so the formatter knows not to add
// certain articles to the names.
//
// Also note that if no ending punctuation is given, then a period is added
// automatically. The sentence is also capitalized if was not already.
func Log(s string, args ...interface{}) *LogEvent {
	objects := []interface{}{} // subject is always objects[0]

	replace := func(match string) string {
		// If we've consumed all the args, but still have more format
		// specifiers, we use a missing format value string.
		if match[0] == '%' && len(args) == 0 {
			return fmt.Sprintf("%%!%s(MISSING)", match[1:])
		}

		var arg interface{}

		switch match {
		case "%s":
			arg, args = args[0], args[1:]
			objects = append(objects, arg)
			objects[0] = arg
			return getName(arg)
		case "%o":
			arg, args = args[0], args[1:]
			objects = append(objects, arg)
			if arg == objects[0] {
				return getReflexive(arg)
			}
			return getName(arg)
		case "%v":
			arg, args = args[0], args[1:]
			return getVerb(arg, objects[0])
		case "%x":
			arg, args = args[0], args[1:]
			return fmt.Sprintf("%v", arg)
		}

		return getVerb(match[1:len(match)-1], objects[0])
	}

	s = strings.Replace(s, "%%", "%", -1)
	s = makeSentence(formatRE.ReplaceAllStringFunc(s, replace))

	// If we have remaining arguments, append an extra argument format value to
	// the result string.
	if len(args) > 0 {
		argstrs := make([]string, len(args))
		for i, arg := range args {
			argstrs[i] = fmt.Sprintf("%v", arg)
		}
		extra := strings.Join(argstrs, " ")
		s = fmt.Sprintf("%s%%!(EXTRA %s)", s, extra)
	}

	return &LogEvent{s}
}

// Data needed by Fmt helper functions. These should be regarded as constants.
var (
	formatRE             = regexp.MustCompile("%s|%o|%v|%x|<.+?>")
	articles             = []string{"the", "a"}
	irregularVerbsSecond = map[string]string{
		"be": "are",
	}
	irregularVerbsThird = map[string]string{
		"do":     "does",
		"be":     "is",
		"have":   "has",
		"can":    "can",
		"cannot": "cannot",
		"could":  "could",
		"may":    "may",
		"must":   "must",
		"shall":  "shall",
		"should": "should",
		"will":   "will",
		"would":  "would",
	}
	esEndings      = []string{"ch", "sh", "ss", "x", "o"}
	endPunctuation = []string{".", "!", "?"}
)

// includesArticle returns true if the given name starts with an article.
func includesArticle(name string) bool {
	for _, article := range articles {
		if strings.HasPrefix(name, article+" ") {
			return true
		}
	}
	return false
}

// isUniqueName returns true if the name starts with an uppercase rune.
func isUniqueName(name string) bool {
	return unicode.IsUpper([]rune(name)[0])
}

// getArticle prepends the article 'the' when the name is both non-unique and
// does not already contain an article.
func getArticle(name string) string {
	if name == "you" || includesArticle(name) || isUniqueName(name) {
		return name
	}
	return "the " + name
}

// getName returns the string name for a particular noun. If the noun is an
// Entity, then a NameQuery is attempted. If that fails, then fmt.Sprintf
// is used instead. If needed, the article 'the' is prepended to the name.
func getName(noun interface{}) string {
	v := NameEvent{}
	if entity, ok := noun.(Entity); ok {
		entity.Handle(&v)
	}
	if v.Name == "" {
		v.Name = fmt.Sprintf("%v", noun)
	}
	return getArticle(v.Name)
}

// getReflexive turns a noun into a reflexive pronoun.
func getReflexive(noun interface{}) string {
	name := getName(noun)
	if name == "you" {
		return "yourself"
	}
	// TODO handle gender for uniques
	return "itself"
}

// conjuageSecond conjugates a verb in the second person tense.
func conjugateSecond(verb string) string {
	if conjugated, irregular := irregularVerbsSecond[verb]; irregular {
		return conjugated
	}
	return verb
}

// conjugateThird conjugates a verb in the third person tense.
func conjugateThird(verb string) string {
	if congugated, irregular := irregularVerbsThird[verb]; irregular {
		return congugated
	}
	for _, ending := range esEndings {
		if strings.HasSuffix(verb, ending) {
			return verb + "es"
		}
	}
	if strings.HasSuffix(verb, "y") {
		return verb[:len(verb)-1] + "ies"
	}
	return verb + "s"
}

// getVerb conjugates a verb given a particular subject.
func getVerb(verb, subject interface{}) string {
	// TODO should verbs also use getName?
	phrase := strings.Fields(fmt.Sprintf("%v", verb))
	// TODO Handle both plural and singular nouns (currently just singular)
	if getName(subject) == "you" {
		phrase[0] = conjugateSecond(phrase[0])
	} else {
		phrase[0] = conjugateThird(phrase[0])
	}
	return strings.Join(phrase, " ")
}

// makeSentence ensures proper capitalization and punctuation.
func makeSentence(s string) string {
	s = strings.ToUpper(s[:1]) + s[1:]
	for _, punctuation := range endPunctuation {
		if strings.HasSuffix(s, punctuation) {
			return s
		}
	}
	return s + "."
}

// TODO Add possessives to the format language
