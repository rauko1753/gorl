// Package gorl is a (somewhat) generic roguelike toolkit for Go.
package gorl

// Event is a message sent to an Entity.
type Event interface{}

// Component processes Events for an Entity.
type Component interface {
	Process(Event)
}

// Entity is a single game object represented as a collection of Component.
type Entity interface {
	Handle(Event)
}

// ComponentFunc is function which implements Component.
type ComponentFunc func(Event)

// Process implements Component for ComponentFunc
func (c ComponentFunc) Process(v Event) {
	c(v)
}

// ComponentSlice is an Entity which is a slice of Components.
type ComponentSlice []Component

// Handle implements Entity for ComponentSlice by sending an Event to each
// Component for processing in order.
func (e ComponentSlice) Handle(v Event) {
	for _, c := range e {
		c.Process(v)
	}
}

// AddComponent appends a Component to the ComponentSlice.
func (e *ComponentSlice) AddComponent(c Component) {
	*e = append(*e, c)
}

// Tile is an Entity representing a single square in a map.
type Tile struct {
	Face     Glyph
	Pass     bool
	Lite     bool
	Offset   Offset
	Adjacent map[Offset]*Tile
	Occupant Entity

	ComponentSlice
}

// NewTile creates a new Tile with no neighbors or occupant.
func NewTile(o Offset) *Tile {
	return &Tile{Char('.'), true, true, o, make(map[Offset]*Tile), nil, nil}
}

// Handle implements Entity for Tile
func (e *Tile) Handle(v Event) {
	switch v := v.(type) {
	case *RenderEvent:
		v.Render = e.Face
		if e.Occupant != nil {
			e.Occupant.Handle(v)
		}
	case *MoveEvent:
		if bumped := v.Dest.Occupant; bumped != nil {
			e.Occupant.Handle(&BumpEvent{bumped})
		} else if v.Dest.Pass {
			e.Occupant.Handle(v)
			v.Dest.Handle(&OccupyEvent{e.Occupant})
			e.Occupant = nil
		} else {
			e.Occupant.Handle(&CollideEvent{v.Dest})
		}
	case *OccupyEvent:
		e.Occupant = v.Occupant
	}

	e.ComponentSlice.Handle(v)
}

// ActEvent is an Event triggering an Entity action.
type ActEvent struct{}

// ScheduleEvent is an Event triggering an Entity scheduling.
type ScheduleEvent struct {
	Delta float64
}

// UnscheduleEvent is an Event triggering an Entity unscheduling.
type UnscheduleEvent struct{}

// LogEvent is an Event requesting that a message be logged.
type LogEvent struct {
	Msg string
}

// RenderEvent is an Event querying an Entity for a Glyph to render.
type RenderEvent struct {
	Render Glyph
}

// MoveEvent is an Event triggering an Entity move to a new Tile.
type MoveEvent struct {
	Dest *Tile
}

// BumpEvent is an Event in which one Entity bumps another.
type BumpEvent struct {
	Bumped Entity
}

// OccupyEvent is an Event informing about a new Entity occupant.
type OccupyEvent struct {
	Occupant Entity
}

// CollideEvent is an Event in which an Event collides with a Tile obstacle.
type CollideEvent struct {
	Obstacle *Tile
}

// NameEvent is an Event querying an Entity for its name.
type NameEvent struct {
	Name string
}

// FoVEvent is an Event querying an Entity for a field of view.
type FoVEvent struct {
	FoV map[Offset]*Tile
}

// LoSEvent is an Event querying an Entity whether a Tile is in line of sight.
type LoSEvent struct {
	Target  *Tile
	Visible bool
}
