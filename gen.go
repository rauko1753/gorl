package gorl

// GenTile generates Tile for various map generators. The typical use case is to
// wrap other GenTile types for use with NewTileGrid.
type GenTile func(o Offset) *Tile

// GenTileGrid creates a 2D grid of Tile using the given GenTile.
func GenTileGrid(cols, rows int, f GenTile) []*Tile {
	backing := make([]*Tile, cols*rows)

	tiles := make([][]*Tile, cols)
	for x := 0; x < cols; x++ {
		tiles[x] = backing[x*rows : (x+1)*rows]
		for y := 0; y < rows; y++ {
			tiles[x][y] = f(Offset{x, y})
		}
	}

	link := func(x, y, dx, dy int) {
		nx, ny := x+dx, y+dy
		if 0 <= nx && nx < cols && 0 <= ny && ny < rows {
			tiles[x][y].Adjacent[Offset{X: dx, Y: dy}] = tiles[nx][ny]
		}
	}

	for x := 0; x < cols; x++ {
		for y := 0; y < rows; y++ {
			link(x, y, 1, 1)
			link(x, y, 1, 0)
			link(x, y, 1, -1)
			link(x, y, 0, 1)
			link(x, y, 0, -1)
			link(x, y, -1, 1)
			link(x, y, -1, 0)
			link(x, y, -1, -1)
		}
	}

	return backing
}

// GenTileMutate mutates extisting Tiles to modify existing maps.
type GenTileMutate func(*Tile)

// GenFence mutates any Tile which is not 8-connected.
func GenFence(tiles []*Tile, f GenTileMutate) {
	for _, tile := range tiles {
		if len(tile.Adjacent) < 8 {
			f(tile)
		}
	}
}

// GenTransform applies a transformation to an entire map.
func GenTransform(tiles []*Tile, f GenTileMutate) {
	for _, tile := range tiles {
		f(tile)
	}
}

// TODO Add temperature map based on latitude and elevation
// TODO Add precipitation map based on latitude and elevation
