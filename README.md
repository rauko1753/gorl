GoRL
====
GoRL is a (somewhat) generic roguelike toolkit for Go. It is intended mostly
for development of [Sticks and Stones](http://gitlab.com/rauko1753/stones), but
should be generally useful for various roguelikes. It is written entirely in
[Go](https://golang.org) and utilizes
[termbox-go](https://github.com/nsf/termbox-go) for display so it should be
usable on all major platforms.

Features
========
* Game Model
  * Simple but powerful Entity-Component system
  * Graph based maps
  * Delta clock for Entity scheduling
* Procedural Generation
  * World generation
  * Maze generation
  * Dungeon generation
  * Data-driven name generator
* AI
  * Graph-based pathfinding
  * Dijkstra fields
  * Behavior trees (forthcoming)
* Dice
  * Fast and high quality xorshift1024 random source
  * Roguelike-specific RNG functions
* UI
  * Terminal abstraction based on termbox-go
  * Composable terminal-based UI elements
* FoV
  * Extremely fast field of view computation
  * Fast line of sight checks which match field of view
