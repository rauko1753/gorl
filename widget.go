package gorl

import (
	"fmt"
)

// Widget represents something which can draw itself on the screen.
type Widget interface {
	Update()
}

// Screen is a collection of Widget.
type Screen []Widget

// Update runs Update on each ScreenElement of the Screen.
func (s Screen) Update() {
	TermClear()
	for _, w := range s {
		w.Update()
	}
	TermRefresh()
}

// Window serves as a base for various Widget which need relateive drawing.
type Window struct {
	X, Y, W, H int
}

// DrawRel performs a TermDraw relative to the location of the Widget.
func (w *Window) DrawRel(x, y int, g Glyph) {
	if 0 <= x && x <= w.W && 0 <= y && y < w.H {
		TermDraw(x+w.X, y+w.Y, g)
	}
}

// TextWidget is a Widget which displays dynamically bound text on screen.
type TextWidget struct {
	Window
	Binding func() string
}

// NewTextWidget creates a new TextWidget with the given binding.
func NewTextWidget(x, y, w, h int, binding func() string) *TextWidget {
	return &TextWidget{Window{x, y, w, h}, binding}
}

// Update draws the bound text on screen.
func (w *TextWidget) Update() {
	x, y := 0, 0
	for _, ch := range w.Binding() {
		if ch == '\n' {
			x, y = 0, y+1
		} else {
			w.DrawRel(x, y, Char(ch)) // TODO Add color to TextWidget
			x++
		}
	}
}

// logmsg is a cached message in LogWidget.
type logmsg struct {
	Text  string
	Count int
	Seen  bool
}

// String implements fmt.Stringer for logmsg.
func (m *logmsg) String() string {
	if m.Count == 1 {
		return m.Text
	}
	return fmt.Sprintf("%s (x%d)", m.Text, m.Count)
}

// LogWidget is a Widget which stores and displays log messages. LogWidget is
// also a Component which accepts LogEvent.
type LogWidget struct {
	Window
	cache []*logmsg
}

// NewLogWidget creates a new empty LogWidget.
func NewLogWidget(x, y, w, h int) *LogWidget {
	return &LogWidget{Window{x, y, w, h}, nil}
}

// Log places a message in the LogWidget cache.
func (w *LogWidget) Log(msg string) {
	if len(msg) == 0 {
		return
	}

	last := len(w.cache) - 1
	// if cache is empty, or last message text was different than this one
	if last < 0 || w.cache[last].Text != msg {
		w.cache = append(w.cache, &logmsg{msg, 1, false})
		// truncate cache if too long to show in the Widget Window
		if len(w.cache) > w.H {
			w.cache = w.cache[len(w.cache)-w.H:]
		}
	} else {
		// duplicate text, so just reuse last message
		w.cache[last].Count++
		w.cache[last].Seen = false
	}
}

// Update draws the cached log messages on screen.
func (w *LogWidget) Update() {
	for y, msg := range w.cache {
		var fg Color // TODO Add custom colors in LogWidget
		if msg.Seen {
			fg = ColorLightBlack
		} else {
			fg = ColorWhite
		}

		// note we assume no newlines, unlike TextWidget.
		for x, ch := range msg.String() {
			w.DrawRel(x, y, ColoredChar(ch, fg))
		}

		// just displayed the message, so next time should be seen
		msg.Seen = true
	}
}

// Process implements Component for LogWidget.
func (w *LogWidget) Process(v Event) {
	switch v := v.(type) {
	case *LogEvent:
		w.Log(v.Msg)
	}
}

// NewLogger creates a Component which responds to LogEvent whenever the
// condtion returns true.
func (w *LogWidget) NewLogger(condition func() bool) Component {
	return ComponentFunc(func(v Event) {
		switch v := v.(type) {
		case *LogEvent:
			if condition() {
				w.Log(v.Msg)
			}
		}
	})
}

// CameraWidget is a Widget which displays an Entity field of view on screen.
type CameraWidget struct {
	Window
	Camera Entity
}

// NewCameraWidget creates a new CameraWidget with the given camera Entity.
func NewCameraWidget(x, y, w, h int, camera Entity) *CameraWidget {
	return &CameraWidget{Window{x, y, w, h}, camera}
}

// Update draws the camera field of view on screen.
func (w *CameraWidget) Update() {
	v := FoVEvent{}
	w.Camera.Handle(&v)
	cx, cy := w.W/2, w.H/2

	for off, tile := range v.FoV {
		v := RenderEvent{}
		tile.Handle(&v)
		w.DrawRel(cx+off.X, cy+off.Y, v.Render)
	}
}

// TODO Add memory to CameraWidget
